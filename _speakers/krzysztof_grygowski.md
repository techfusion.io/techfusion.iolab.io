---
name: Krzysztof Grygowski
website:
image: /assets/images/season01/speakers/
twitter: 
linkedin-in: 
facebook: 
flickr: 
---
Krzysztof opowiadał o tym co działo się za sceną podczas tworzenia współczesnych filmów. 

Jego wystąpieniu przewodziło hasło "Is the film just an artist's vision or is technology creating new reality?".

Krzysztof w swoim portfolio ma udział w produkcjach takich jak "300: Rise of an Empire", "World War Z", "Percy Jackson: Sea of Monsters".