---
name: Marek Madej
website: 
image: /assets/images/season07/speakers/
twitter: 
linkedin-in: 
facebook: 
flickr: 
---
Marek to digital artist, który głównie zajmuje się koncepcją artystyczną i ilustratorstwem. W momencie meetup'u pracował dla CD Project Red, gdzie był odpowiedzialny m.in. za grę "Wiedźmin 3: Dziki Gon".

W przeszłości pracował dla kilku firm takich jak Platige Image, Human Ark, Metropolis Software i zajmował się drobnym freelancing'iem dla wielu innych.

Na spotkaniu opowiadał o typowym dniu pracy w studiu Project Red z perspektywny artysty koncepcyjnego; jak to wygląda, z jakimi wyzwaniami spotyka się na co dzień i na czym ogólnie polega projektowanie gier.