---
name: Piotr Buliński
website:
image: /assets/images/season01/speakers/
twitter: 
linkedin-in: 
facebook: 
flickr: 
---
Piotr pracując jako developer w Berlińskiej firmie **castLabs GmbH** opowiadał o technologiach, usługach i oprogramowaniu, które jego firma wykorzystywała przy tworzeniu ekosystemu UltraViolet (<http://www.uvvu.com/>) - DVD/BluRay w chmurze.