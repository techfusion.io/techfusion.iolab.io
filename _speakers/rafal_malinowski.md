---
name: Rafał Malinowski
website:
image: /assets/images/season02/speakers/
twitter: 
linkedin-in: 
facebook: 
flickr: 
---
Wystąpienie Rafała wybrzmiało pod hasłem "sharding is hard". 

Od ogółu do szczegółu. Od tego czym jest "sharding" do meritum, czyli algorytmów wykorzystywanych w "shardowaniu" danych. 
