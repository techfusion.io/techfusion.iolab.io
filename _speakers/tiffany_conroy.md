---
name: Tiffany Conroy
website:
image: /assets/images/season01/speakers/
twitter: 
linkedin-in: 
facebook:
flickr: 
---
Tiffany pracowała w <https://soundcloud.com> na stanowisku "Frontend Developer and Interaction Designer". 

Na spotkaniu opowiadała o tym jak przeprojektować/podzielić "single-page app" w kilka mniejszych, łatwiejszych do zarządzania aplikacji.