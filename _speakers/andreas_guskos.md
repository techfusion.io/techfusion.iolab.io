---
name: Andreas Guskos
website:
image: /assets/images/season07/speakers/
twitter: 
linkedin-in: 
facebook: 
flickr: 
---
Profesor Andreas Guskos, który prowadzi Pracownię Wystawiennictwa i Nowych Technologii w Projektowaniu (Akademia Sztuki w Szczecinie) jest twórcą bardzo nowatorskich projektów: wirtualnego świata „Aheilos” oraz corocznego projektu o nazwie „Medea”. Specjalizuje się w wykorzystaniu interaktywnej, wirtualnej przestrzeni do kratywnej pracy.

Na spotkaniu opowiedział m.in. o budowie silników gier i ich interfejsach w kontekście architekta i nauczyciela. Podał również przykłady aktualnych i potencjalnych aplikacji nowej generacji i wspomniał o historii ich podłoża.
Na koniec przedstawił środowisko stworzonego przez siebie świata "Aheilos".
