---
name: Marek Dopiera
website: 
image: /assets/images/season06/speakers
twitter: 
linkedin-in: 
facebook:
flickr: 
---
Marek opowiedział o nierelacyjnej bazie danych Google'a: Bigtable.

Oprócz opisu jej funkcjonalności, mówił o tym jak jest zaimplementowana i dlaczego akurat tak. Ponadto przedstawił jakie problemy rozwiązuje, a jakich nie oraz jak wpisuje się w ekosystem innych technologii w infrastrukturze Google'a.