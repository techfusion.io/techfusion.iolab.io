---
order: 1
name: Matt Harasymczuk
website:
image: /assets/images/season08/speakers/
twitter: 
linkedin-in: 
facebook:
flickr: 
---
Matt jest bioastronautycznym naukowcem, pilotem, inżynierem przestrzeni kosmicznej i oprogramowania, a także zapalonym skoczkiem spadochronowym, nurkiem i medykiem wojskowym (TCCC, ACLS). Ówcześnie brał udział w badaniach "Moon and Mars" Europejskiej Agencji Kosmicznej. 

Na naszym meet'upie przedstawił nieziemski temat "Software Engineering i technologie high-tech w przemyśle kosmicznym i astronautyce".
