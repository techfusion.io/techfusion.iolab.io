---
name: Jakub Lasota
website:
image: 
twitter: 
linkedin-in: 
facebook: 
flickr: 
---
Jakub opowiedział o stanie światowej gospodarki funkcjonującej w cieniu ówczesnej "recesji bilansowej" i "pułapki płynności", a także o związku nowych technologii z rynkami finansowymi i powstawaniem baniek spekulacyjnych.

Ponadto, poddał weryfikacji hipotezę o istnieniu oligopolu w ekonomi jako nauce, która wbrew dynamice zmian w innych dziedzinach, wciąż tkwi w XVIII wieku wraz z Adamem Smith'em.
