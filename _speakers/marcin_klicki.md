---
name: Marcin Klicki
website:
image: /assets/images/season07/speakers
twitter: 
linkedin-in: 
facebook: 
flickr: 
---
Pierwsze poważne zlecenie Marcin wykonywał dla Platige Image w 2004. Następnie w 2007 podjął się stałej współpracy z tą firmą. Przez 11 lat pracował nad wieloma komercyjnymi projektami, kilkoma animacjami, filmami i cinematicami gier dla takich firm jak Human Ark, Ars Thanea i Platige Image. W czerwcu 2012 roku rozpoczął pracę swoich marzeń w CD Project Red.

Marcin opowiadał nam o modelowaniu 3D i oprogramowaniu z tym związanym takim jak Zbrush i Wings3D. Poruszył także temat restrykcji nałożonych przez silnik gry z punktu widzenia kreatywnej wolności twórcy.
