JEKYLL  := 3.8.5
PORT    := 4001
TFDIR   := ${CURDIR}

start: serve

build:
	docker exec -ti tf jekyll build || docker run --volume="${TFDIR}:/srv/jekyll" -it jekyll/jekyll:${JEKYLL} jekyll build

serve:
	docker start -a tf || docker run --name tf -p ${PORT}:4000 --volume="${TFDIR}:/srv/jekyll" -it jekyll/jekyll:${JEKYLL} jekyll serve --watch

stop:
	docker stop tf

clean:
	docker stop tf || exit 0
	docker rm tf || exit 0
	rm -r _site || exit 0
