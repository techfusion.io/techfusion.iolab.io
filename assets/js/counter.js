export const counterToDate = (id, date) => {
    const days = countRemainingDays(date);
    
    if (days < 10) {
        document.getElementById(id).innerHTML = `0${days}`;
    } else if (days > 0) {
        document.getElementById(id).innerHTML = days;
    } else if (days < 0) {
        document.getElementById(id).innerHTML = "$$";
    }
}

const countRemainingDays = (date) => {
    const difference = date - new Date();
    return Math.floor(difference / (1000 * 60 * 60 * 24));
}