import { counterToDate } from './counter.js';
import { registerAnimation } from './register-animation.js';
import { uniteGallery } from './gallery.js';

const date = document.getElementById('counter').dataset.date;

counterToDate('counter', new Date(date));
registerAnimation();
uniteGallery();