export const uniteGallery = () => {
    jQuery(document).ready(() => {
        jQuery("#gallery").unitegallery({
            theme_enable_text_panel: false,
            theme_enable_fullscreen_button: false,
            theme_enable_hidepanel_button: false,
            gallery_width: `100%`,
            gallery_height: 600,
            slider_control_swipe: false,
            slider_control_zoom: false,
        });
    });
}