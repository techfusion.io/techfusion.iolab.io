# TechFusion
![Logo TechFusion](https://i.imgur.com/1tkgS5T.png)

## Table of contents
* [Intro](#intro)
* [About TechFusion](#about-techfusion)
* [Url](#url)
* [How to add a new speaker](#how-to-add-a-new-speaker)
* [How to add a new season](#how-to-add-a-new-season)
* [Technologies](#technologies)
* [Pluggins](#pluggins)

## Intro 
Website of TechFusion � event for IT and technology enthusiasts. Contains description of current season and the archive of past seasons with a photo gallery and featured lectures.

## About TechFusion
TechFusion is an initiative to bring together all people passionate about technology AND those who actually are practitioners (please note the underlined AND). Don't get us wrong, we love marketing/social media gurus, lobbyists/evangelists/activists and other less technical specialities - without them the tech industry wouldn�t be what it is today - and there are a lot of initiatives around technology that are in fundamentals less technical (like NetCamp) - but we would like to create a unique place that would enable code/admin/design/music/devops gurus to meet, easily find someone with whom we can share our experiences and from whom we can learn. Thus, by attending TechFusion meetings you agree to a social contract not only to attend but also to PARTICIPATE (talk, comment, share or do your own presentations). We are firmly convinced that if we open up, you will open up too.

## Url
[TechFusion.io](https://techfusion.io)

### How to add a new speaker 
1. Find folder _speakers and enter it	
2. Create a new.md file there, named as the speaker with underscore between name and surname

	Example:
    
	>  jeremy_smith.md
3. Copy those fields and fill them:
```
---
name: 
website:   
image: 
twitter:
linkedin-in:     
facebook: 
flickr:
---
```	
4. Website and social links fields (twitter, facebook, etc.) are optional and can be left empty. Make sure you have provided the appropriate link's destination:

	Example:
    
	>  image: /assets/images/season01/speakers/nameOfFile.jpg
5. Under Front Matter section (first section between --- signs) write few words about speaker. 
6. Done!

### How to add sponsor
1. Find folder _sponsors
2. Create .md file named with name of sponsor:
	Example:
    
	>  google.md
3. Copy those fields and fill them:
```
---
name: 
website: 
image: 
---
```
4. If you want to add created sponsor to season - look at instruction below.
5. Done!

### How to add season
>>>
home page will display the nearest season **with future date**, seasons that have already taken place, will be displayed in the archive automatically
>>>

1. Find folder _seasons	
2. Create .md file named with number of season:

	Example:
    
	> 	1.md 
3. Copy those fields and fill them:
```	
---	
title: 
number: 	
speakers:
	- Speaker name
	- Speaker name
	- Speaker name
date: 
about:
	- paragraph
    	- paragraph
	- paragraph
photos: 
    - url: /assets/images/season01/gallery/s01p01.jpg
    - url: /assets/images/season01/gallery/s01p02.jpg
sponsors: 
	- Sponsor name
	- Speaker name
---
```
4. Season fields explanation:
* title: "Season " + number of season.
* number: Season number (should be greater by value of 1 than the number of previous season).
* speakers: Starting with ' - ' point out name and last name of speakers of that season.
* date: Add date in YYYY-MM-DD format. If your date will be placed in the past, that season will be automatically directed to the archive. From the future seasons, the one closest to the current date will be displayed on home page. 
* about: Starting with ' - ' add new paragraphs with description of season.
* photos: Images displayed in gallery section. Enter url of image. It will be displayed in two versions - thumbnail in grayscale and after preview large, colorfull photo.
* sponsors: Starting with ' - ' point out names of each sponsor of this season (with a same name you created previously in 'How to add sponsor' instruction)
5. Done!

## Technologies
Project is created with:
* jQuery v3.3.1
* Bootstrap 4.3.1 
* Ruby Sass v3.7.3
* Jekyll v3.8.5
* JavaScript(ES+6) & HTML5 & CS3

## Plugins
* SimpleLightbox v1.16.1